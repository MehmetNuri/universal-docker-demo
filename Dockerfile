FROM openjdk:11-jdk
VOLUME /tmp
ADD /target/*.jar universal-xx.jar
ENTRYPOINT ["java","-jar","/universal-xx.jar"]
