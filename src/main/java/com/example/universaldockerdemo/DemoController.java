package com.example.universaldockerdemo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Mehmet Nuri ÖZTÜRK on 24.09.2021 .
 */
@RestController
public class DemoController {

    @GetMapping
    public String hello() {

        return "Universal Docker";
    }
}
