package com.example.universaldockerdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UniversalDockerDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(UniversalDockerDemoApplication.class, args);
    }

}
